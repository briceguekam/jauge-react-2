import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import {blue, deepOrange, red, blueGrey} from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography'
const styles = theme => ({
    content: {
        background : blueGrey[50],
        width : "100%",
        paddingTop : 20,
        paddingBottom : 20,
        textAlign : "center"
    },
    typo : {
        color : blueGrey[700]
    }
});
const data = {

};

class Helpers extends React.Component {
    render() {
        return (
            <div className={this.props.classes.content}>
                <Typography className={this.props.classes.typo} variant={"h4"} align={"center"}>Title Of helps</Typography>
                <Typography paragraph className={this.props.classes.typo}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cumque est impedit minus nobis porro quod tenetur. Accusantium aut dolorum et illum maxime nihil, non nostrum nulla obcaecati quam sint!
                </Typography>

            </div>
        );
    }
}

export default withStyles(styles)(Helpers);