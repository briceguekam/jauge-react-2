// @flow
import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
//import routes from '../constants/routes';
import styles from './Home.css';
import Content from './private/Content'
import {blue, deepOrange, orange, blueGrey} from '@material-ui/core/colors';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blue[600]
    },
    secondary: {
      main: orange[400]
    },
    background : blue[50],
    text : blue[50],
  }
});

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className={styles.container} data-tid="container">
          <Content/>
        </div>
      </MuiThemeProvider>
    );
  }
}
