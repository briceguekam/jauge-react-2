// @flow
import React, { Component } from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Typography from "@material-ui/core/es/Typography/Typography";
// feuille de style utiliser pour personaliser le composant Jauge
const styles = theme => ({
  parent : {
    backgroundColor:theme.palette.background,
    padding : 10,
    height : 200,
    margin : "0px 20px",
    borderRadius:5,
    position : "relative"
  },
  child : {
    position : "absolute",
    top : "50%",
    left : "50%",
    transform : "translate(-50%, -50%)"
  },
  typo : {
    color : "#000"
  }
});

class Messages extends Component {

  render() {
    return (
      <div className={this.props.classes.parent}>
          <div className={this.props.classes.child}>
              <Typography variant={"h5"} className={this.props.classes.typo}>
                {this.props.messages}
              </Typography>
          </div>
      </div>
    );
  }
}
export default withStyles(styles)(Messages);
